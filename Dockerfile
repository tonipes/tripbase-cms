FROM python:latest
MAINTAINER Toni Pesola

RUN apt-get update && apt-get install -y --no-install-recommends \
		git \
	  && rm -rf /var/lib/apt/lists/*

RUN pip install requests

COPY docker-hook.py /

COPY pull.sh /

COPY run.sh /

RUN chmod +x /docker-hook.py

RUN chmod +x /pull.sh

RUN chmod +x /run.sh

WORKDIR /

ENTRYPOINT ["/run.sh"]
