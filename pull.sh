#!/bin/bash
echo "Pulling repo at $CMS_GIT_PATH"
cd "$CMS_GIT_PATH"
git fetch --all
git reset --hard origin/$CMS_GIT_BRANCH
git pull
echo "Repository pulled"
exit
