#!/bin/bash
echo "Starting..."

if [ -z "$CMS_GIT_PATH" ]; then
  echo "No CMS_GIT_PATH defined, exiting!"
  exit
fi

if [ -z "$CMS_GIT_REPO" ]; then
  echo "No CMS_GIT_REPO defined, exiting!"
  exit
fi

if [ ! -d "$CMS_GIT_PATH" ]; then
  echo "Creating folder: $CMS_GIT_PATH"
  mkdir -p $CMS_GIT_PATH
fi
if [ ! -d "$CMS_GIT_PATH/.git" ]; then
  echo "Cloning to $CMS_GIT_PATH"
  git clone  --recursive -b $CMS_GIT_BRANCH $CMS_GIT_REPO $CMS_GIT_PATH
  echo "Clone done."
  cd $CMS_GIT_PATH && git checkout $CMS_GIT_BRANCH
  git reset --hard origin/$CMS_GIT_BRANCH
  echo "First pull done"
else
  echo ".git already exists! Using existing repository"
fi

echo "Starting hook listener at $CMS_LISTENER_ADDR:$CMS_LISTENER_PORT"

/docker-hook.py -c "/pull.sh" -t "$CMS_LISTENER_AUTH_TOKEN" --addr "$CMS_LISTENER_ADDR" --port "$CMS_LISTENER_PORT"
